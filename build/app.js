require("source-map-support").install();
(function(e, a) { for(var i in a) e[i] = a[i]; }(exports, /******/ (function(modules) { // webpackBootstrap
/******/ 	// The module cache
/******/ 	var installedModules = {};
/******/
/******/ 	// The require function
/******/ 	function __webpack_require__(moduleId) {
/******/
/******/ 		// Check if module is in cache
/******/ 		if(installedModules[moduleId])
/******/ 			return installedModules[moduleId].exports;
/******/
/******/ 		// Create a new module (and put it into the cache)
/******/ 		var module = installedModules[moduleId] = {
/******/ 			exports: {},
/******/ 			id: moduleId,
/******/ 			loaded: false
/******/ 		};
/******/
/******/ 		// Execute the module function
/******/ 		modules[moduleId].call(module.exports, module, module.exports, __webpack_require__);
/******/
/******/ 		// Flag the module as loaded
/******/ 		module.loaded = true;
/******/
/******/ 		// Return the exports of the module
/******/ 		return module.exports;
/******/ 	}
/******/
/******/
/******/ 	// expose the modules object (__webpack_modules__)
/******/ 	__webpack_require__.m = modules;
/******/
/******/ 	// expose the module cache
/******/ 	__webpack_require__.c = installedModules;
/******/
/******/ 	// __webpack_public_path__
/******/ 	__webpack_require__.p = "";
/******/
/******/ 	// Load entry module and return exports
/******/ 	return __webpack_require__(0);
/******/ })
/************************************************************************/
/******/ ([
/* 0 */
/***/ function(module, exports, __webpack_require__) {

	'use strict';
	
	__webpack_require__(1);
	
	var _server = __webpack_require__(2);
	
	var _server2 = _interopRequireDefault(_server);
	
	var _server3 = __webpack_require__(13);
	
	var _server4 = _interopRequireDefault(_server3);
	
	var _server5 = __webpack_require__(34);
	
	var _server6 = _interopRequireDefault(_server5);
	
	function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }
	
	(0, _server2.default)(_server4.default, _server6.default);

/***/ },
/* 1 */
/***/ function(module, exports) {

	module.exports = require("babel-polyfill");

/***/ },
/* 2 */
/***/ function(module, exports, __webpack_require__) {

	/* WEBPACK VAR INJECTION */(function(__dirname) {'use strict';
	
	Object.defineProperty(exports, "__esModule", {
	  value: true
	});
	
	var _express = __webpack_require__(3);
	
	var _express2 = _interopRequireDefault(_express);
	
	var _compression = __webpack_require__(4);
	
	var _compression2 = _interopRequireDefault(_compression);
	
	var _bodyParser = __webpack_require__(5);
	
	var _bodyParser2 = _interopRequireDefault(_bodyParser);
	
	var _cookieParser = __webpack_require__(6);
	
	var _cookieParser2 = _interopRequireDefault(_cookieParser);
	
	var _path = __webpack_require__(7);
	
	var _path2 = _interopRequireDefault(_path);
	
	var _noCacheMiddleware = __webpack_require__(8);
	
	var _noCacheMiddleware2 = _interopRequireDefault(_noCacheMiddleware);
	
	var _errorsMiddleware = __webpack_require__(9);
	
	var _errorsMiddleware2 = _interopRequireDefault(_errorsMiddleware);
	
	var _loggerMiddleware = __webpack_require__(10);
	
	var _loggerMiddleware2 = _interopRequireDefault(_loggerMiddleware);
	
	function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }
	
	/* eslint no-console: 0 */
	
	exports.default = function (initter, config) {
	
	  var server = (0, _express2.default)();
	
	  var serverEnv = server.get('env');
	
	  (0, _loggerMiddleware2.default)(server, serverEnv, config.bundle);
	
	  server.use((0, _compression2.default)());
	
	  server.use(_bodyParser2.default.json());
	  server.use(_bodyParser2.default.urlencoded({ extended: true }));
	
	  server.use((0, _cookieParser2.default)());
	
	  server.use(_express2.default.static(_path2.default.join(__dirname, 'public')));
	
	  server.use('/', _noCacheMiddleware2.default, initter);
	
	  server.set('view engine', 'jade');
	  server.set('views', _path2.default.join(__dirname, 'app'));
	  server.use(_errorsMiddleware2.default);
	
	  server.set('port', config.appPort);
	
	  server.listen(server.get('port'), function () {
	    var bundle = config.bundle;
	    var env = config.env;
	
	    var expressPort = this.address().port;
	    console.log('=> 🚀  Express ' + bundle + ' ' + env + ' server is running on port ' + expressPort);
	  });
	};
	/* WEBPACK VAR INJECTION */}.call(exports, ""))

/***/ },
/* 3 */
/***/ function(module, exports) {

	module.exports = require("express");

/***/ },
/* 4 */
/***/ function(module, exports) {

	module.exports = require("compression");

/***/ },
/* 5 */
/***/ function(module, exports) {

	module.exports = require("body-parser");

/***/ },
/* 6 */
/***/ function(module, exports) {

	module.exports = require("cookie-parser");

/***/ },
/* 7 */
/***/ function(module, exports) {

	module.exports = require("path");

/***/ },
/* 8 */
/***/ function(module, exports) {

	'use strict';
	
	Object.defineProperty(exports, "__esModule", {
	  value: true
	});
	
	exports.default = function (req, res, next) {
	
	  res.header('Cache-Control', 'private, no-cache, no-store, must-revalidate');
	  res.header('Expires', '-1');
	  res.header('Pragma', 'no-cache');
	  next();
	};

/***/ },
/* 9 */
/***/ function(module, exports) {

	'use strict';
	
	Object.defineProperty(exports, "__esModule", {
	  value: true
	});
	
	exports.default = function (err, req, res, next) {
	
	  res.status(err.status);
	
	  if (req.accepts('html')) {
	    var errPartial = parseInt(err.status, 10) >= 500 ? 500 : err.status;
	    res.render('errors/' + errPartial, { url: req.url, err: err.status });
	  } else {
	    res.sendStatus(err.status);
	  }
	};

/***/ },
/* 10 */
/***/ function(module, exports, __webpack_require__) {

	'use strict';
	
	Object.defineProperty(exports, "__esModule", {
	  value: true
	});
	
	var _morgan = __webpack_require__(11);
	
	var _morgan2 = _interopRequireDefault(_morgan);
	
	var _fs = __webpack_require__(12);
	
	var _fs2 = _interopRequireDefault(_fs);
	
	function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }
	
	exports.default = function (app, env, bundle) {
	
	  if (env === 'production') {
	    var logPath = process.cwd() + '/logs/production.' + bundle + '.log';
	    var logStream = _fs2.default.createWriteStream(logPath, { flags: 'a' });
	    app.use((0, _morgan2.default)('combined', { stream: logStream }));
	  } else {
	    app.use((0, _morgan2.default)('dev'));
	  }
	};

/***/ },
/* 11 */
/***/ function(module, exports) {

	module.exports = require("morgan");

/***/ },
/* 12 */
/***/ function(module, exports) {

	module.exports = require("fs");

/***/ },
/* 13 */
/***/ function(module, exports, __webpack_require__) {

	'use strict';
	
	Object.defineProperty(exports, "__esModule", {
	  value: true
	});
	
	var _server = __webpack_require__(14);
	
	var _server2 = _interopRequireDefault(_server);
	
	var _server3 = __webpack_require__(34);
	
	var _server4 = _interopRequireDefault(_server3);
	
	var _routes = __webpack_require__(35);
	
	var _routes2 = _interopRequireDefault(_routes);
	
	var _reducers = __webpack_require__(71);
	
	var _reducers2 = _interopRequireDefault(_reducers);
	
	var _Head = __webpack_require__(74);
	
	var _Head2 = _interopRequireDefault(_Head);
	
	var _authActions = __webpack_require__(44);
	
	var authActions = _interopRequireWildcard(_authActions);
	
	var _getAsset = __webpack_require__(79);
	
	var _getAsset2 = _interopRequireDefault(_getAsset);
	
	var _setCookieDomain = __webpack_require__(81);
	
	var _setCookieDomain2 = _interopRequireDefault(_setCookieDomain);
	
	function _interopRequireWildcard(obj) { if (obj && obj.__esModule) { return obj; } else { var newObj = {}; if (obj != null) { for (var key in obj) { if (Object.prototype.hasOwnProperty.call(obj, key)) newObj[key] = obj[key]; } } newObj.default = obj; return newObj; } }
	
	function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }
	
	exports.default = function (req, res, next) {
	  var bundle = _server4.default.bundle;
	  var facebookAppId = _server4.default.facebookAppId;
	
	
	  var cookieDomain = (0, _setCookieDomain2.default)(req.headers.host);
	
	  var params = {
	    bundle: bundle,
	    routes: _routes2.default,
	    reducers: _reducers2.default,
	    Head: _Head2.default,
	    authActions: authActions,
	    cookieDomain: cookieDomain,
	    facebookAppId: facebookAppId,
	    locals: {
	      jsAsset: (0, _getAsset2.default)(bundle, 'js'),
	      cssAsset: (0, _getAsset2.default)(bundle, 'css'),
	      vendorAsset: (0, _getAsset2.default)('vendor', 'js')
	    }
	  };
	
	  (0, _server2.default)(req, res, next, params);
	};

/***/ },
/* 14 */
/***/ function(module, exports, __webpack_require__) {

	'use strict';
	
	Object.defineProperty(exports, "__esModule", {
	  value: true
	});
	
	var _react = __webpack_require__(15);
	
	var _react2 = _interopRequireDefault(_react);
	
	var _server = __webpack_require__(16);
	
	var _server2 = _interopRequireDefault(_server);
	
	var _reactRouter = __webpack_require__(17);
	
	var _reactRedux = __webpack_require__(18);
	
	var _transitImmutableJs = __webpack_require__(19);
	
	var _transitImmutableJs2 = _interopRequireDefault(_transitImmutableJs);
	
	var _serializeJavascript = __webpack_require__(20);
	
	var _serializeJavascript2 = _interopRequireDefault(_serializeJavascript);
	
	var _jade = __webpack_require__(21);
	
	var _jade2 = _interopRequireDefault(_jade);
	
	var _store = __webpack_require__(22);
	
	var _store2 = _interopRequireDefault(_store);
	
	var _populateState = __webpack_require__(25);
	
	var _populateState2 = _interopRequireDefault(_populateState);
	
	var _getRouteName = __webpack_require__(26);
	
	var _getRouteName2 = _interopRequireDefault(_getRouteName);
	
	var _Auth = __webpack_require__(27);
	
	var _Auth2 = _interopRequireDefault(_Auth);
	
	var _apiCall = __webpack_require__(31);
	
	var _apiCall2 = _interopRequireDefault(_apiCall);
	
	var _chunkManifest = __webpack_require__(33);
	
	var _chunkManifest2 = _interopRequireDefault(_chunkManifest);
	
	function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }
	
	function _asyncToGenerator(fn) { return function () { var gen = fn.apply(this, arguments); return new Promise(function (resolve, reject) { function step(key, arg) { try { var info = gen[key](arg); var value = info.value; } catch (error) { reject(error); return; } if (info.done) { resolve(value); } else { return Promise.resolve(value).then(function (value) { return step("next", value); }, function (err) { return step("throw", err); }); } } return step("next"); }); }; }
	
	exports.default = function () {
	  var _ref = _asyncToGenerator(regeneratorRuntime.mark(function _callee2(req, res, next, params) {
	    var reducers, store, location, routes, authAgent, appHost, apiHost;
	    return regeneratorRuntime.wrap(function _callee2$(_context2) {
	      while (1) {
	        switch (_context2.prev = _context2.next) {
	          case 0:
	            reducers = params.reducers;
	            store = (0, _store2.default)({ reducers: reducers });
	            location = req.url;
	            routes = params.routes(store);
	            authAgent = new _Auth2.default(req, params.cookieDomain);
	            appHost = req.protocol + '://' + req.headers.host;
	            apiHost = req.protocol + '://api.' + req.headers.host;
	
	            if (!authAgent.isLoggedIn()) {
	              _context2.next = 10;
	              break;
	            }
	
	            _context2.next = 10;
	            return (0, _apiCall2.default)({
	              method: 'GET',
	              host: apiHost,
	              path: '/auth/preflight',
	              auth: authAgent.getAuthHeaders()
	            }).then(function (response) {
	              return store.dispatch(params.authActions.setLoggedInState(authAgent.getLogin()));
	            }).catch(function (response) {
	              return response.status === 401 ? authAgent.logout() : false;
	            });
	
	          case 10:
	
	            (0, _reactRouter.match)({ routes: routes, location: location }, function () {
	              var _ref2 = _asyncToGenerator(regeneratorRuntime.mark(function _callee(error, redirect, props) {
	                var err, state, bundle, locals, Head, facebookAppId, layout, html;
	                return regeneratorRuntime.wrap(function _callee$(_context) {
	                  while (1) {
	                    switch (_context.prev = _context.next) {
	                      case 0:
	                        if (!error) {
	                          _context.next = 5;
	                          break;
	                        }
	
	                        err = new Error();
	
	                        err.status = error.status || 500;
	                        err.message = error.message;
	                        return _context.abrupt('return', next(err));
	
	                      case 5:
	                        if (!redirect) {
	                          _context.next = 7;
	                          break;
	                        }
	
	                        return _context.abrupt('return', res.redirect(302, redirect.pathname + redirect.search));
	
	                      case 7:
	                        if (!props) {
	                          _context.next = 28;
	                          break;
	                        }
	
	                        _context.prev = 8;
	                        _context.next = 11;
	                        return (0, _populateState2.default)(props.components, {
	                          apiHost: apiHost,
	                          auth: authAgent.getAuthHeaders(),
	                          dispatch: store.dispatch,
	                          location: props.location,
	                          params: props.params
	                        });
	
	                      case 11:
	                        state = store.getState();
	                        bundle = params.bundle;
	                        locals = params.locals;
	                        Head = params.Head;
	                        facebookAppId = params.facebookAppId;
	
	
	                        locals.head = _server2.default.renderToStaticMarkup(_react2.default.createElement(Head, {
	                          state: state,
	                          appHost: appHost,
	                          facebookAppId: facebookAppId,
	                          fullPath: req.url,
	                          route: (0, _getRouteName2.default)(props.routes),
	                          location: props.location,
	                          params: props.params,
	                          cssAsset: locals.cssAsset
	                        }));
	
	                        locals.body = _server2.default.renderToString(_react2.default.createElement(
	                          _reactRedux.Provider,
	                          { store: store },
	                          _react2.default.createElement(_reactRouter.RouterContext, props)
	                        ));
	
	                        locals.chunks = (0, _serializeJavascript2.default)(_chunkManifest2.default);
	                        locals.data = (0, _serializeJavascript2.default)(_transitImmutableJs2.default.toJSON(state));
	
	                        layout = process.cwd() + '/app/bundles/' + bundle + '/layouts/Layout.jade';
	                        html = _jade2.default.compileFile(layout, { pretty: false })(locals);
	                        return _context.abrupt('return', res.send(html));
	
	                      case 25:
	                        _context.prev = 25;
	                        _context.t0 = _context['catch'](8);
	                        return _context.abrupt('return', res.status(500).send(_context.t0.stack));
	
	                      case 28:
	                        return _context.abrupt('return', res.send(404, 'Not found'));
	
	                      case 29:
	                      case 'end':
	                        return _context.stop();
	                    }
	                  }
	                }, _callee, undefined, [[8, 25]]);
	              }));
	
	              return function (_x5, _x6, _x7) {
	                return _ref2.apply(this, arguments);
	              };
	            }());
	
	          case 11:
	          case 'end':
	            return _context2.stop();
	        }
	      }
	    }, _callee2, undefined);
	  }));
	
	  return function (_x, _x2, _x3, _x4) {
	    return _ref.apply(this, arguments);
	  };
	}();

/***/ },
/* 15 */
/***/ function(module, exports) {

	module.exports = require("react");

/***/ },
/* 16 */
/***/ function(module, exports) {

	module.exports = require("react-dom/server");

/***/ },
/* 17 */
/***/ function(module, exports) {

	module.exports = require("react-router");

/***/ },
/* 18 */
/***/ function(module, exports) {

	module.exports = require("react-redux");

/***/ },
/* 19 */
/***/ function(module, exports) {

	module.exports = require("transit-immutable-js");

/***/ },
/* 20 */
/***/ function(module, exports) {

	module.exports = require("serialize-javascript");

/***/ },
/* 21 */
/***/ function(module, exports) {

	module.exports = require("jade");

/***/ },
/* 22 */
/***/ function(module, exports, __webpack_require__) {

	'use strict';
	
	Object.defineProperty(exports, "__esModule", {
	  value: true
	});
	
	var _redux = __webpack_require__(23);
	
	var _reduxThunk = __webpack_require__(24);
	
	var _reduxThunk2 = _interopRequireDefault(_reduxThunk);
	
	function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }
	
	exports.default = function (params) {
	  var reducers = params.reducers;
	  var initialState = params.initialState;
	
	
	  var combinedReducers = (0, _redux.combineReducers)(reducers);
	  var composedStore = (0, _redux.compose)((0, _redux.applyMiddleware)(_reduxThunk2.default));
	  var storeCreator = composedStore(_redux.createStore);
	
	  var args = initialState ? [combinedReducers, initialState] : [combinedReducers];
	
	  return storeCreator.apply(undefined, args);
	};

/***/ },
/* 23 */
/***/ function(module, exports) {

	module.exports = require("redux");

/***/ },
/* 24 */
/***/ function(module, exports) {

	module.exports = require("redux-thunk");

/***/ },
/* 25 */
/***/ function(module, exports) {

	"use strict";
	
	Object.defineProperty(exports, "__esModule", {
	  value: true
	});
	
	// In case of huge app where amount of data is big & it's changing all the time
	// you might want to pass here only the current route (from server initter)
	// and run `fetchData` method(s) only for current route
	
	exports.default = function (components) {
	  for (var _len = arguments.length, args = Array(_len > 1 ? _len - 1 : 0), _key = 1; _key < _len; _key++) {
	    args[_key - 1] = arguments[_key];
	  }
	
	  return Promise.all(components.filter(function (component) {
	    return component.fetchData;
	  }).map(function (component) {
	    return component.fetchData.apply(component, args);
	  }));
	};

/***/ },
/* 26 */
/***/ function(module, exports) {

	"use strict";
	
	Object.defineProperty(exports, "__esModule", {
	  value: true
	});
	
	exports.default = function (routes) {
	  return routes[routes.length - 1].name;
	};

/***/ },
/* 27 */
/***/ function(module, exports, __webpack_require__) {

	'use strict';
	
	Object.defineProperty(exports, "__esModule", {
	  value: true
	});
	exports.default = undefined;
	
	var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();
	
	var _server = __webpack_require__(28);
	
	var _server2 = _interopRequireDefault(_server);
	
	var _cookies = __webpack_require__(29);
	
	var _cookies2 = _interopRequireDefault(_cookies);
	
	function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }
	
	function _defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }
	
	function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }
	
	var Auth = function () {
	  function Auth(context, domain) {
	    _classCallCheck(this, Auth);
	
	    this.cookie = (0, _cookies2.default)(context);
	
	    this.setCookieParams = function (months) {
	      var monthsFromNow = function monthsFromNow(quantity) {
	        var now = new Date();
	        return new Date(now.setMonth(now.getMonth() + quantity));
	      };
	
	      var params = {};
	
	      params.httpOnly = false;
	
	      if (domain) params.domain = domain;
	      if (months) params.expires = monthsFromNow(months);
	
	      return params;
	    };
	  }
	
	  _createClass(Auth, [{
	    key: 'login',
	    value: function login(email, token) {
	      var params = arguments.length <= 2 || arguments[2] === undefined ? {} : arguments[2];
	
	      var cookieParams = params.sessionOnly ? this.setCookieParams() : this.setCookieParams(6);
	
	      this.cookie.set(_server2.default.loginCookie, email, cookieParams);
	      this.cookie.set(_server2.default.tokenCookie, token, cookieParams);
	
	      if (params.callback) params.callback();
	    }
	  }, {
	    key: 'logout',
	    value: function logout(callback) {
	      this.cookie.set(_server2.default.loginCookie, 'bye-bye', this.setCookieParams(-1));
	      this.cookie.set(_server2.default.tokenCookie, 'bye-bye', this.setCookieParams(-1));
	
	      if (callback) return callback();
	    }
	  }, {
	    key: 'getLogin',
	    value: function getLogin() {
	      return this.cookie.get(_server2.default.loginCookie);
	    }
	  }, {
	    key: 'getToken',
	    value: function getToken() {
	      return this.cookie.get(_server2.default.tokenCookie);
	    }
	  }, {
	    key: 'getAuthHeaders',
	    value: function getAuthHeaders() {
	      if (this.getLogin() && this.getToken()) {
	        var _ref;
	
	        return _ref = {}, _defineProperty(_ref, _server2.default.loginHeader, this.getLogin()), _defineProperty(_ref, _server2.default.tokenHeader, this.getToken()), _ref;
	      }
	
	      return false;
	    }
	  }, {
	    key: 'isLoggedIn',
	    value: function isLoggedIn() {
	      var cookie = this.cookie;
	
	      return !!(cookie.get(_server2.default.loginCookie) && cookie.get(_server2.default.tokenCookie));
	    }
	  }]);
	
	  return Auth;
	}();
	
	exports.default = Auth;

/***/ },
/* 28 */
/***/ function(module, exports, __webpack_require__) {

	'use strict';
	
	Object.defineProperty(exports, "__esModule", {
	  value: true
	});
	/* eslint no-process-env: 0 */
	
	exports.default = {
	
	  env: ("production") || 'development',
	
	  devPort: 3001,
	
	  apiName: '<%= name %>',
	  apiVersion: 'v1',
	
	  apiPath: '/api',
	
	  loginCookie: 'api_login',
	  tokenCookie: 'api_token',
	
	  loginHeader: 'X-User-Email',
	  tokenHeader: 'X-User-Token'
	
	};

/***/ },
/* 29 */
/***/ function(module, exports, __webpack_require__) {

	'use strict';
	
	Object.defineProperty(exports, "__esModule", {
	  value: true
	});
	
	var _cookie = __webpack_require__(30);
	
	var _cookie2 = _interopRequireDefault(_cookie);
	
	function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }
	
	exports.default = function (context) {
	
	  if (process.browser) {
	
	    return {
	      get: function get(name) {
	        return _cookie2.default.parse(context.cookie)[name];
	      },
	      set: function set(name, value, options) {
	        context.cookie = _cookie2.default.serialize(name, value, options);
	      }
	    };
	  } else {
	
	    return {
	      get: function get(name) {
	        return context.cookies[name];
	      },
	      set: function set(name, value, options) {
	        context.res.cookie(name, value, options);
	      }
	    };
	  }
	};

/***/ },
/* 30 */
/***/ function(module, exports) {

	module.exports = require("cookie");

/***/ },
/* 31 */
/***/ function(module, exports, __webpack_require__) {

	'use strict';
	
	Object.defineProperty(exports, "__esModule", {
	  value: true
	});
	
	var _axios = __webpack_require__(32);
	
	var _axios2 = _interopRequireDefault(_axios);
	
	var _server = __webpack_require__(28);
	
	var _server2 = _interopRequireDefault(_server);
	
	function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }
	
	// TODO: Refactor
	
	exports.default = function (params) {
	
	  var method = params.method;
	  var url = '' + (params.host || _server2.default.apiPath) + params.path;
	  var responseType = 'json';
	
	  var headers = {
	    'Content-Type': 'application/json',
	    'Accept': 'application/vnd.' + _server2.default.apiName + '.' + _server2.default.apiVersion + '+json'
	  };
	
	  if (params.auth) Object.assign(headers, params.auth);
	
	  var requestParams = { method: method, url: url, responseType: responseType, headers: headers };
	
	  if (params.data) requestParams.data = params.data;
	
	  return (0, _axios2.default)(requestParams);
	};

/***/ },
/* 32 */
/***/ function(module, exports) {

	module.exports = require("axios");

/***/ },
/* 33 */
/***/ function(module, exports) {

	module.exports = {};

/***/ },
/* 34 */
/***/ function(module, exports, __webpack_require__) {

	'use strict';
	
	Object.defineProperty(exports, "__esModule", {
	  value: true
	});
	
	var _server = __webpack_require__(28);
	
	var _server2 = _interopRequireDefault(_server);
	
	function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }
	
	var appConfig = {
	
	  bundle: 'app',
	  appPort: ({"NODE_ENV":"production"}).APP_PORT || 3500,
	
	  googleAnalyticsId: 'UA-XXXXXXXX-Y',
	  facebookAppId: '123456789012345'
	
	}; /* eslint no-process-env: 0 */
	
	exports.default = Object.assign({}, _server2.default, appConfig);

/***/ },
/* 35 */
/***/ function(module, exports, __webpack_require__) {

	'use strict';
	
	Object.defineProperty(exports, "__esModule", {
	  value: true
	});
	
	var _extends = Object.assign || function (target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i]; for (var key in source) { if (Object.prototype.hasOwnProperty.call(source, key)) { target[key] = source[key]; } } } return target; };
	
	var _react = __webpack_require__(15);
	
	var _react2 = _interopRequireDefault(_react);
	
	var _reactRouter = __webpack_require__(17);
	
	var _AppContainer = __webpack_require__(36);
	
	var _AppContainer2 = _interopRequireDefault(_AppContainer);
	
	var _DummiesContainer = __webpack_require__(48);
	
	var _DummiesContainer2 = _interopRequireDefault(_DummiesContainer);
	
	var _LoginContainer = __webpack_require__(61);
	
	var _LoginContainer2 = _interopRequireDefault(_LoginContainer);
	
	var _Logout = __webpack_require__(68);
	
	var _Logout2 = _interopRequireDefault(_Logout);
	
	var _NotFound = __webpack_require__(69);
	
	var _NotFound2 = _interopRequireDefault(_NotFound);
	
	function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }
	
	exports.default = function (store) {
	
	  var loginHooks = {
	    store: store,
	    onEnter: _LoginContainer2.default.WrappedComponent.checkAuth
	  };
	
	  return _react2.default.createElement(
	    _reactRouter.Route,
	    { name: 'app', component: _AppContainer2.default },
	    _react2.default.createElement(_reactRouter.Route, { name: 'dummies', path: '/', component: _DummiesContainer2.default }),
	    _react2.default.createElement(_reactRouter.Route, _extends({ name: 'login', path: '/login', component: _LoginContainer2.default }, loginHooks)),
	    _react2.default.createElement(_reactRouter.Route, { name: 'logout', path: '/logout', component: _Logout2.default }),
	    _react2.default.createElement(_reactRouter.Route, { name: 'not-found', path: '*', component: _NotFound2.default })
	  );
	};

/***/ },
/* 36 */
/***/ function(module, exports, __webpack_require__) {

	'use strict';
	
	Object.defineProperty(exports, "__esModule", {
	  value: true
	});
	exports.default = undefined;
	
	var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();
	
	var _dec, _class, _class2, _temp;
	
	var _react = __webpack_require__(15);
	
	var _react2 = _interopRequireDefault(_react);
	
	var _immutable = __webpack_require__(37);
	
	var _immutable2 = _interopRequireDefault(_immutable);
	
	var _redux = __webpack_require__(23);
	
	var _reactRedux = __webpack_require__(18);
	
	var _Layout = __webpack_require__(38);
	
	var _Layout2 = _interopRequireDefault(_Layout);
	
	var _authActions = __webpack_require__(44);
	
	var authActions = _interopRequireWildcard(_authActions);
	
	function _interopRequireWildcard(obj) { if (obj && obj.__esModule) { return obj; } else { var newObj = {}; if (obj != null) { for (var key in obj) { if (Object.prototype.hasOwnProperty.call(obj, key)) newObj[key] = obj[key]; } } newObj.default = obj; return newObj; } }
	
	function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }
	
	function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }
	
	function _possibleConstructorReturn(self, call) { if (!self) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return call && (typeof call === "object" || typeof call === "function") ? call : self; }
	
	function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function, not " + typeof superClass); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, enumerable: false, writable: true, configurable: true } }); if (superClass) Object.setPrototypeOf ? Object.setPrototypeOf(subClass, superClass) : subClass.__proto__ = superClass; }
	
	var stateMap = function stateMap(state) {
	  return {
	    $authStore: state.$authStore
	  };
	};
	
	var actionsMap = function actionsMap(dispatch) {
	  return {
	    authActions: (0, _redux.bindActionCreators)(authActions, dispatch)
	  };
	};
	
	var App = (_dec = (0, _reactRedux.connect)(stateMap, actionsMap), _dec(_class = (_temp = _class2 = function (_React$Component) {
	  _inherits(App, _React$Component);
	
	  function App() {
	    _classCallCheck(this, App);
	
	    return _possibleConstructorReturn(this, Object.getPrototypeOf(App).apply(this, arguments));
	  }
	
	  _createClass(App, [{
	    key: 'render',
	    value: function render() {
	      return _react2.default.createElement(_Layout2.default, this.props);
	    }
	  }]);
	
	  return App;
	}(_react2.default.Component), _class2.propTypes = {
	  $authStore: _react.PropTypes.instanceOf(_immutable2.default.Map).isRequired
	}, _temp)) || _class);
	exports.default = App;

/***/ },
/* 37 */
/***/ function(module, exports) {

	module.exports = require("immutable");

/***/ },
/* 38 */
/***/ function(module, exports, __webpack_require__) {

	'use strict';
	
	Object.defineProperty(exports, "__esModule", {
	  value: true
	});
	exports.default = undefined;
	
	var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();
	
	var _class, _temp;
	
	var _react = __webpack_require__(15);
	
	var _react2 = _interopRequireDefault(_react);
	
	var _Header = __webpack_require__(39);
	
	var _Header2 = _interopRequireDefault(_Header);
	
	var _Footer = __webpack_require__(41);
	
	var _Footer2 = _interopRequireDefault(_Footer);
	
	__webpack_require__(43);
	
	function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }
	
	function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }
	
	function _possibleConstructorReturn(self, call) { if (!self) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return call && (typeof call === "object" || typeof call === "function") ? call : self; }
	
	function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function, not " + typeof superClass); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, enumerable: false, writable: true, configurable: true } }); if (superClass) Object.setPrototypeOf ? Object.setPrototypeOf(subClass, superClass) : subClass.__proto__ = superClass; }
	
	var Layout = (_temp = _class = function (_React$Component) {
	  _inherits(Layout, _React$Component);
	
	  function Layout() {
	    _classCallCheck(this, Layout);
	
	    return _possibleConstructorReturn(this, Object.getPrototypeOf(Layout).apply(this, arguments));
	  }
	
	  _createClass(Layout, [{
	    key: 'render',
	    value: function render() {
	      return _react2.default.createElement(
	        'section',
	        { id: 'layout' },
	        _react2.default.createElement(_Header2.default, null),
	        this.props.children,
	        _react2.default.createElement(_Footer2.default, null)
	      );
	    }
	  }]);
	
	  return Layout;
	}(_react2.default.Component), _class.propTypes = {
	  children: _react.PropTypes.object
	}, _temp);
	exports.default = Layout;

/***/ },
/* 39 */
/***/ function(module, exports, __webpack_require__) {

	'use strict';
	
	Object.defineProperty(exports, "__esModule", {
	  value: true
	});
	exports.default = undefined;
	
	var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();
	
	var _react = __webpack_require__(15);
	
	var _react2 = _interopRequireDefault(_react);
	
	var _reactRouter = __webpack_require__(17);
	
	__webpack_require__(40);
	
	function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }
	
	function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }
	
	function _possibleConstructorReturn(self, call) { if (!self) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return call && (typeof call === "object" || typeof call === "function") ? call : self; }
	
	function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function, not " + typeof superClass); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, enumerable: false, writable: true, configurable: true } }); if (superClass) Object.setPrototypeOf ? Object.setPrototypeOf(subClass, superClass) : subClass.__proto__ = superClass; }
	
	var Header = function (_React$Component) {
	  _inherits(Header, _React$Component);
	
	  function Header() {
	    _classCallCheck(this, Header);
	
	    return _possibleConstructorReturn(this, Object.getPrototypeOf(Header).apply(this, arguments));
	  }
	
	  _createClass(Header, [{
	    key: 'render',
	    value: function render() {
	      return _react2.default.createElement(
	        'header',
	        null,
	        _react2.default.createElement(
	          'h1',
	          null,
	          _react2.default.createElement(
	            _reactRouter.Link,
	            { to: '/' },
	            'Isomorphic Flux'
	          )
	        )
	      );
	    }
	  }]);
	
	  return Header;
	}(_react2.default.Component);
	
	exports.default = Header;

/***/ },
/* 40 */
/***/ function(module, exports) {



/***/ },
/* 41 */
/***/ function(module, exports, __webpack_require__) {

	'use strict';
	
	Object.defineProperty(exports, "__esModule", {
	  value: true
	});
	exports.default = undefined;
	
	var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();
	
	var _react = __webpack_require__(15);
	
	var _react2 = _interopRequireDefault(_react);
	
	var _Footer = __webpack_require__(42);
	
	var _Footer2 = _interopRequireDefault(_Footer);
	
	function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }
	
	function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }
	
	function _possibleConstructorReturn(self, call) { if (!self) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return call && (typeof call === "object" || typeof call === "function") ? call : self; }
	
	function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function, not " + typeof superClass); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, enumerable: false, writable: true, configurable: true } }); if (superClass) Object.setPrototypeOf ? Object.setPrototypeOf(subClass, superClass) : subClass.__proto__ = superClass; }
	
	var Footer = function (_React$Component) {
	  _inherits(Footer, _React$Component);
	
	  function Footer() {
	    _classCallCheck(this, Footer);
	
	    return _possibleConstructorReturn(this, Object.getPrototypeOf(Footer).apply(this, arguments));
	  }
	
	  _createClass(Footer, [{
	    key: 'render',
	    value: function render() {
	      return _react2.default.createElement(
	        'footer',
	        null,
	        _react2.default.createElement(
	          'div',
	          { className: _Footer2.default.inner },
	          _react2.default.createElement(
	            'div',
	            { className: _Footer2.default.text },
	            _react2.default.createElement(
	              'a',
	              { href: 'https://facebook.github.io/react', target: '_blank' },
	              'React'
	            ),
	            '·',
	            _react2.default.createElement(
	              'a',
	              { href: 'https://github.com/gaearon/redux', target: '_blank' },
	              'Redux'
	            ),
	            '·',
	            _react2.default.createElement(
	              'a',
	              { href: 'http://rackt.github.io/react-router', target: '_blank' },
	              'React Router'
	            ),
	            '·',
	            _react2.default.createElement(
	              'a',
	              { href: 'http://expressjs.com', target: '_blank' },
	              'Express'
	            ),
	            '·',
	            _react2.default.createElement(
	              'a',
	              { href: 'https://nodejs.org', target: '_blank' },
	              'Node.js'
	            ),
	            '·',
	            _react2.default.createElement(
	              'a',
	              { href: 'http://rubyonrails.org', target: '_blank' },
	              'Ruby on Rails'
	            )
	          )
	        )
	      );
	    }
	  }]);
	
	  return Footer;
	}(_react2.default.Component);
	
	exports.default = Footer;

/***/ },
/* 42 */
/***/ function(module, exports) {

	module.exports = {
		"layout": "Footer__layout__3tmQm",
		"inner": "Footer__inner__1zKIx",
		"text": "Footer__text__3L5PK"
	};

/***/ },
/* 43 */
/***/ function(module, exports) {

	module.exports = {
		"app": "Layout__app__2CAuh",
		"layout": "Layout__layout__Uf8Qd"
	};

/***/ },
/* 44 */
/***/ function(module, exports, __webpack_require__) {

	'use strict';
	
	Object.defineProperty(exports, "__esModule", {
	  value: true
	});
	exports.setLoggedInState = setLoggedInState;
	exports.login = login;
	exports.logout = logout;
	
	var _apiCall = __webpack_require__(31);
	
	var _apiCall2 = _interopRequireDefault(_apiCall);
	
	var _authPaths = __webpack_require__(45);
	
	var _authConstants = __webpack_require__(46);
	
	var _authConstants2 = _interopRequireDefault(_authConstants);
	
	function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }
	
	function setLoggedInState(user) {
	  return {
	    type: _authConstants2.default.AUTH_LOGGED_IN,
	    user: user
	  };
	}
	
	function login(_ref) {
	  var authData = _ref.authData;
	  var authAgent = _ref.authAgent;
	  var history = _ref.history;
	
	  return function (dispatch) {
	
	    dispatch({
	      type: _authConstants2.default.AUTH_LOGIN_REQUESTED
	    });
	
	    return (0, _apiCall2.default)({
	      method: 'POST',
	      path: _authPaths.apiPaths.loginPath(),
	      data: authData
	    }).then(function (res) {
	      var _res$data$user = res.data.user;
	      var user = _res$data$user['email'];
	      var token = _res$data$user['authentication_token'];
	
	
	      authAgent.login(user, token, {
	        sessionOnly: false,
	        callback: function callback() {
	          dispatch({
	            type: _authConstants2.default.AUTH_LOGIN_SUCCEED,
	            user: user
	          });
	          if (!history.goBack()) history.pushState(null, '/');
	        }
	      });
	    }).catch(function (res) {
	      dispatch({
	        type: _authConstants2.default.AUTH_LOGIN_FAILED,
	        errors: {
	          code: res.status,
	          data: res.data
	        }
	      });
	    });
	  };
	}
	
	function logout(_ref2) {
	  var authAgent = _ref2.authAgent;
	  var history = _ref2.history;
	  var nextLocation = _ref2.nextLocation;
	
	  return function (dispatch) {
	    authAgent.logout(function () {
	      dispatch({
	        type: _authConstants2.default.AUTH_LOGGED_OUT
	      });
	
	      history.pushState({ nextLocation: nextLocation }, _authPaths.paths.logoutPath());
	    });
	  };
	}

/***/ },
/* 45 */
/***/ function(module, exports) {

	'use strict';
	
	Object.defineProperty(exports, "__esModule", {
	  value: true
	});
	var paths = exports.paths = {
	  logoutPath: function logoutPath() {
	    return '/logout';
	  }
	};
	
	var apiPaths = exports.apiPaths = {
	  loginPath: function loginPath() {
	    return '/login';
	  }
	};

/***/ },
/* 46 */
/***/ function(module, exports, __webpack_require__) {

	'use strict';
	
	Object.defineProperty(exports, "__esModule", {
	  value: true
	});
	
	var _mirrorCreator = __webpack_require__(47);
	
	var _mirrorCreator2 = _interopRequireDefault(_mirrorCreator);
	
	function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }
	
	exports.default = (0, _mirrorCreator2.default)(['AUTH_LOGIN_REQUESTED', 'AUTH_LOGIN_SUCCEED', 'AUTH_LOGIN_FAILED', 'AUTH_LOGGED_IN', 'AUTH_LOGGED_OUT']);

/***/ },
/* 47 */
/***/ function(module, exports) {

	module.exports = require("mirror-creator");

/***/ },
/* 48 */
/***/ function(module, exports, __webpack_require__) {

	'use strict';
	
	Object.defineProperty(exports, "__esModule", {
	  value: true
	});
	exports.default = undefined;
	
	var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();
	
	var _dec, _dec2, _class;
	
	var _react = __webpack_require__(15);
	
	var _react2 = _interopRequireDefault(_react);
	
	var _redux = __webpack_require__(23);
	
	var _reactRedux = __webpack_require__(18);
	
	var _fetchData = __webpack_require__(49);
	
	var _fetchData2 = _interopRequireDefault(_fetchData);
	
	var _authActions = __webpack_require__(44);
	
	var authActions = _interopRequireWildcard(_authActions);
	
	var _dummiesActions = __webpack_require__(50);
	
	var dummiesActions = _interopRequireWildcard(_dummiesActions);
	
	var _Dummies = __webpack_require__(55);
	
	var _Dummies2 = _interopRequireDefault(_Dummies);
	
	function _interopRequireWildcard(obj) { if (obj && obj.__esModule) { return obj; } else { var newObj = {}; if (obj != null) { for (var key in obj) { if (Object.prototype.hasOwnProperty.call(obj, key)) newObj[key] = obj[key]; } } newObj.default = obj; return newObj; } }
	
	function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }
	
	function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }
	
	function _possibleConstructorReturn(self, call) { if (!self) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return call && (typeof call === "object" || typeof call === "function") ? call : self; }
	
	function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function, not " + typeof superClass); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, enumerable: false, writable: true, configurable: true } }); if (superClass) Object.setPrototypeOf ? Object.setPrototypeOf(subClass, superClass) : subClass.__proto__ = superClass; }
	
	var stateMap = function stateMap(state) {
	  return {
	    $authStore: state.$authStore,
	    $dummiesStore: state.$dummiesStore
	  };
	};
	
	var actionsMap = function actionsMap(dispatch) {
	  return {
	    authActions: (0, _redux.bindActionCreators)(authActions, dispatch),
	    dummiesActions: (0, _redux.bindActionCreators)(dummiesActions, dispatch)
	  };
	};
	
	var DummiesContainer = (_dec = (0, _fetchData2.default)(function (_ref) {
	  var apiHost = _ref.apiHost;
	  var auth = _ref.auth;
	  var dispatch = _ref.dispatch;
	  return dispatch(dummiesActions.loadEntities({ apiHost: apiHost, auth: auth }));
	}), _dec2 = (0, _reactRedux.connect)(stateMap, actionsMap), _dec(_class = _dec2(_class = function (_React$Component) {
	  _inherits(DummiesContainer, _React$Component);
	
	  function DummiesContainer() {
	    _classCallCheck(this, DummiesContainer);
	
	    return _possibleConstructorReturn(this, Object.getPrototypeOf(DummiesContainer).apply(this, arguments));
	  }
	
	  _createClass(DummiesContainer, [{
	    key: 'render',
	    value: function render() {
	      return _react2.default.createElement(_Dummies2.default, this.props);
	    }
	  }]);
	
	  return DummiesContainer;
	}(_react2.default.Component)) || _class) || _class);
	exports.default = DummiesContainer;

/***/ },
/* 49 */
/***/ function(module, exports, __webpack_require__) {

	'use strict';
	
	Object.defineProperty(exports, "__esModule", {
	  value: true
	});
	
	var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();
	
	var _react = __webpack_require__(15);
	
	var _react2 = _interopRequireDefault(_react);
	
	function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }
	
	function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }
	
	function _possibleConstructorReturn(self, call) { if (!self) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return call && (typeof call === "object" || typeof call === "function") ? call : self; }
	
	function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function, not " + typeof superClass); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, enumerable: false, writable: true, configurable: true } }); if (superClass) Object.setPrototypeOf ? Object.setPrototypeOf(subClass, superClass) : subClass.__proto__ = superClass; }
	
	exports.default = function (fetchData) {
	  return function (DecoratedComponent) {
	    var _class, _temp;
	
	    return _temp = _class = function (_React$Component) {
	      _inherits(FetchDataDecorator, _React$Component);
	
	      function FetchDataDecorator() {
	        _classCallCheck(this, FetchDataDecorator);
	
	        return _possibleConstructorReturn(this, Object.getPrototypeOf(FetchDataDecorator).apply(this, arguments));
	      }
	
	      _createClass(FetchDataDecorator, [{
	        key: 'componentDidMount',
	        value: function componentDidMount() {
	          if (this.props.isInitialRender) return;
	
	          var _props = this.props;
	          var authAgent = _props.authAgent;
	          var location = _props.location;
	          var params = _props.params;
	          var store = _props.store;
	
	
	          fetchData({
	            location: location,
	            params: params,
	            auth: authAgent.getAuthHeaders(),
	            dispatch: store.dispatch
	          });
	        }
	      }, {
	        key: 'render',
	        value: function render() {
	          return _react2.default.createElement(DecoratedComponent, this.props);
	        }
	      }]);
	
	      return FetchDataDecorator;
	    }(_react2.default.Component), _class.fetchData = fetchData, _class.DecoratedComponent = DecoratedComponent, _class.propTypes = {
	      authAgent: _react.PropTypes.shape({
	        getAuthHeaders: _react.PropTypes.func
	      }),
	
	      store: _react.PropTypes.shape({
	        dispatch: _react.PropTypes.func
	      }),
	
	      location: _react.PropTypes.object,
	      params: _react.PropTypes.object,
	      isInitialRender: _react.PropTypes.bool
	    }, _temp;
	  };
	};

/***/ },
/* 50 */
/***/ function(module, exports, __webpack_require__) {

	'use strict';
	
	Object.defineProperty(exports, "__esModule", {
	  value: true
	});
	exports.loadEntities = loadEntities;
	
	var _normalizr = __webpack_require__(51);
	
	var _apiCall = __webpack_require__(31);
	
	var _apiCall2 = _interopRequireDefault(_apiCall);
	
	var _dummiesPaths = __webpack_require__(52);
	
	var _dummiesSchema = __webpack_require__(53);
	
	var _dummiesSchema2 = _interopRequireDefault(_dummiesSchema);
	
	var _dummiesConstants = __webpack_require__(54);
	
	var _dummiesConstants2 = _interopRequireDefault(_dummiesConstants);
	
	function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }
	
	function loadEntities(_ref) {
	  var apiHost = _ref.apiHost;
	  var auth = _ref.auth;
	
	  return function (dispatch) {
	    dispatch({
	      type: _dummiesConstants2.default.DUMMY_LOAD_LIST_REQUESTED
	    });
	
	    return (0, _apiCall2.default)({
	      method: 'GET',
	      path: _dummiesPaths.apiPaths.dummiesPath(),
	      host: apiHost,
	      auth: auth
	    }).then(function (res) {
	      var dummies = res.data.dummies;
	
	      var normalizedData = (0, _normalizr.normalize)(dummies, (0, _normalizr.arrayOf)(_dummiesSchema2.default));
	
	      dispatch({
	        type: _dummiesConstants2.default.DUMMY_LOAD_LIST_SUCCEED,
	        index: normalizedData.result,
	        entities: normalizedData.entities.dummies
	      });
	    }).catch(function (res) {
	      dispatch({
	        type: _dummiesConstants2.default.DUMMY_LOAD_LIST_FAILED,
	        errors: {
	          code: res.status,
	          data: res.data
	        }
	      });
	    });
	  };
	}

/***/ },
/* 51 */
/***/ function(module, exports) {

	module.exports = require("normalizr");

/***/ },
/* 52 */
/***/ function(module, exports) {

	'use strict';
	
	Object.defineProperty(exports, "__esModule", {
	  value: true
	});
	var paths = exports.paths = {};
	
	var apiPaths = exports.apiPaths = {
	  dummiesPath: function dummiesPath() {
	    return '/dummies';
	  }
	};

/***/ },
/* 53 */
/***/ function(module, exports, __webpack_require__) {

	'use strict';
	
	Object.defineProperty(exports, "__esModule", {
	  value: true
	});
	
	var _normalizr = __webpack_require__(51);
	
	var dummy = new _normalizr.Schema('dummies');
	
	exports.default = dummy;

/***/ },
/* 54 */
/***/ function(module, exports, __webpack_require__) {

	'use strict';
	
	Object.defineProperty(exports, "__esModule", {
	  value: true
	});
	
	var _mirrorCreator = __webpack_require__(47);
	
	var _mirrorCreator2 = _interopRequireDefault(_mirrorCreator);
	
	function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }
	
	exports.default = (0, _mirrorCreator2.default)(['DUMMY_LOAD_LIST_REQUESTED', 'DUMMY_LOAD_LIST_SUCCEED', 'DUMMY_LOAD_LIST_FAILED']);

/***/ },
/* 55 */
/***/ function(module, exports, __webpack_require__) {

	'use strict';
	
	Object.defineProperty(exports, "__esModule", {
	  value: true
	});
	exports.default = undefined;
	
	var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();
	
	var _class, _temp;
	
	var _react = __webpack_require__(15);
	
	var _react2 = _interopRequireDefault(_react);
	
	var _immutable = __webpack_require__(37);
	
	var _immutable2 = _interopRequireDefault(_immutable);
	
	var _reactRouter = __webpack_require__(17);
	
	var _classnames = __webpack_require__(56);
	
	var _classnames2 = _interopRequireDefault(_classnames);
	
	var _bindToContext = __webpack_require__(57);
	
	var _bindToContext2 = _interopRequireDefault(_bindToContext);
	
	var _Spinner = __webpack_require__(58);
	
	var _Spinner2 = _interopRequireDefault(_Spinner);
	
	var _dummiesConstants = __webpack_require__(54);
	
	var _dummiesConstants2 = _interopRequireDefault(_dummiesConstants);
	
	var _Dummies = __webpack_require__(60);
	
	var _Dummies2 = _interopRequireDefault(_Dummies);
	
	function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }
	
	function _defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }
	
	function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }
	
	function _possibleConstructorReturn(self, call) { if (!self) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return call && (typeof call === "object" || typeof call === "function") ? call : self; }
	
	function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function, not " + typeof superClass); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, enumerable: false, writable: true, configurable: true } }); if (superClass) Object.setPrototypeOf ? Object.setPrototypeOf(subClass, superClass) : subClass.__proto__ = superClass; }
	
	var Dummies = (_temp = _class = function (_React$Component) {
	  _inherits(Dummies, _React$Component);
	
	  function Dummies(props, context) {
	    _classCallCheck(this, Dummies);
	
	    var _this = _possibleConstructorReturn(this, Object.getPrototypeOf(Dummies).call(this, props, context));
	
	    (0, _bindToContext2.default)(['_handleLogout', '_renderLoggedInButton', '_renderLoggedOutButton', '_renderAuthSection', '_renderDummiesList'], _this);
	    return _this;
	  }
	
	  _createClass(Dummies, [{
	    key: 'componentDidMount',
	    value: function componentDidMount() {
	      var _props = this.props;
	      var loadingProgress = _props.loadingProgress;
	      var setTitle = _props.setTitle;
	
	      loadingProgress.done();
	      setTitle.call(this);
	    }
	  }, {
	    key: 'componentWillReceiveProps',
	    value: function componentWillReceiveProps(newProps) {
	      var $dummiesStore = newProps.$dummiesStore;
	
	      var action = $dummiesStore.get('action');
	
	      if (action === _dummiesConstants2.default.DUMMY_LOAD_LIST_SUCCEED) {
	        var loadingProgress = newProps.loadingProgress;
	        var setTitle = newProps.setTitle;
	
	        loadingProgress.done();
	        setTitle.call(this);
	      }
	    }
	  }, {
	    key: '_handleLogout',
	    value: function _handleLogout() {
	      var _props2 = this.props;
	      var authActions = _props2.authActions;
	      var authAgent = _props2.authAgent;
	      var history = _props2.history;
	      var _props$location = this.props.location;
	      var pathname = _props$location.pathname;
	      var query = _props$location.query;
	
	      var nextLocation = history.createPath(pathname, query);
	
	      authActions.logout({ authAgent: authAgent, history: history, nextLocation: nextLocation });
	    }
	  }, {
	    key: '_renderLoggedInButton',
	    value: function _renderLoggedInButton() {
	      return _react2.default.createElement(
	        'span',
	        { onClick: this._handleLogout, className: _Dummies2.default.authButton },
	        'logout'
	      );
	    }
	  }, {
	    key: '_renderLoggedOutButton',
	    value: function _renderLoggedOutButton() {
	      return _react2.default.createElement(
	        _reactRouter.Link,
	        { to: '/login', className: _Dummies2.default.authButton },
	        'login'
	      );
	    }
	  }, {
	    key: '_renderAuthSection',
	    value: function _renderAuthSection() {
	      var _classNames;
	
	      var $authStore = this.props.$authStore;
	
	
	      var isLoggedIn = $authStore.get('isLoggedIn');
	      var authMessage = isLoggedIn ? 'You\'re logged in!' : 'You\'re logged out';
	      var authImageClassNames = (0, _classnames2.default)((_classNames = {}, _defineProperty(_classNames, _Dummies2.default.loggedIn, isLoggedIn), _defineProperty(_classNames, _Dummies2.default.loggedOut, !isLoggedIn), _classNames));
	
	      return _react2.default.createElement(
	        'div',
	        { className: _Dummies2.default.content },
	        _react2.default.createElement('div', { className: authImageClassNames }),
	        _react2.default.createElement(
	          'div',
	          { className: _Dummies2.default.authMessage },
	          authMessage
	        ),
	        _react2.default.createElement(
	          'div',
	          { className: _Dummies2.default.authButtonWrapper },
	          isLoggedIn ? this._renderLoggedInButton() : this._renderLoggedOutButton()
	        )
	      );
	    }
	  }, {
	    key: '_renderDummiesList',
	    value: function _renderDummiesList() {
	      var $dummiesStore = this.props.$dummiesStore;
	
	      var isLoading = $dummiesStore.get('isLoading');
	
	      var $dummies = $dummiesStore.get('index').map(function (dummyId) {
	        return $dummiesStore.get('entities').get('' + dummyId);
	      });
	
	      return _react2.default.createElement(
	        'div',
	        { className: _Dummies2.default.content },
	        isLoading ? _react2.default.createElement(_Spinner2.default, null) : $dummies.map(function ($dummy) {
	          return _react2.default.createElement(
	            'div',
	            { key: $dummy.get('id'), className: _Dummies2.default.dummyCard },
	            $dummy.get('card')
	          );
	        })
	      );
	    }
	  }, {
	    key: 'render',
	    value: function render() {
	      return _react2.default.createElement(
	        'section',
	        { id: 'dummies', className: _Dummies2.default.dummiesSection },
	        this._renderAuthSection(),
	        this._renderDummiesList()
	      );
	    }
	  }]);
	
	  return Dummies;
	}(_react2.default.Component), _class.propTypes = {
	  $authStore: _react.PropTypes.instanceOf(_immutable2.default.Map).isRequired,
	  $dummiesStore: _react.PropTypes.instanceOf(_immutable2.default.Map).isRequired,
	
	  authActions: _react.PropTypes.shape({
	    logout: _react.PropTypes.func.isRequired
	  }).isRequired,
	
	  authAgent: _react.PropTypes.shape({
	    login: _react.PropTypes.func.isRequired,
	    logout: _react.PropTypes.func.isRequired
	  }),
	
	  location: _react.PropTypes.shape({
	    pathname: _react.PropTypes.string.isRequired,
	    query: _react.PropTypes.object.isRequired
	  }).isRequired,
	
	  history: _react.PropTypes.shape({
	    createPath: _react.PropTypes.func.isRequired
	  }).isRequired,
	
	  loadingProgress: _react.PropTypes.object,
	  setTitle: _react.PropTypes.func
	}, _temp);
	exports.default = Dummies;

/***/ },
/* 56 */
/***/ function(module, exports) {

	module.exports = require("classnames");

/***/ },
/* 57 */
/***/ function(module, exports) {

	"use strict";
	
	Object.defineProperty(exports, "__esModule", {
	  value: true
	});
	
	exports.default = function (methods, context) {
	  methods.forEach(function (method) {
	    return context[method] = context[method].bind(context);
	  });
	};

/***/ },
/* 58 */
/***/ function(module, exports, __webpack_require__) {

	'use strict';
	
	Object.defineProperty(exports, "__esModule", {
	  value: true
	});
	exports.default = undefined;
	
	var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();
	
	var _class, _temp;
	
	var _react = __webpack_require__(15);
	
	var _react2 = _interopRequireDefault(_react);
	
	var _Spinner = __webpack_require__(59);
	
	var _Spinner2 = _interopRequireDefault(_Spinner);
	
	function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }
	
	function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }
	
	function _possibleConstructorReturn(self, call) { if (!self) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return call && (typeof call === "object" || typeof call === "function") ? call : self; }
	
	function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function, not " + typeof superClass); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, enumerable: false, writable: true, configurable: true } }); if (superClass) Object.setPrototypeOf ? Object.setPrototypeOf(subClass, superClass) : subClass.__proto__ = superClass; }
	
	var Spinner = (_temp = _class = function (_React$Component) {
	  _inherits(Spinner, _React$Component);
	
	  function Spinner() {
	    _classCallCheck(this, Spinner);
	
	    return _possibleConstructorReturn(this, Object.getPrototypeOf(Spinner).apply(this, arguments));
	  }
	
	  _createClass(Spinner, [{
	    key: 'render',
	    value: function render() {
	      var color = this.props.color || '#29d';
	      var size = this.props.size || 30;
	      var width = this.props.width || 2;
	      var bgColor = this.props.bgColor;
	
	      var sizePx = size + 'px';
	      var widthPx = width + 'px';
	
	      var wrapperStyle = bgColor ? { backgroundColor: bgColor } : null;
	
	      var sizeStyle = {
	        width: sizePx,
	        height: sizePx
	      };
	
	      var cutStyle = {
	        width: size / 2 + 'px',
	        height: sizePx
	      };
	
	      var donutStyle = {
	        width: sizePx,
	        height: sizePx,
	        border: widthPx + ' solid ' + color,
	        borderRadius: '50%',
	        borderLeftColor: 'transparent',
	        borderBottomColor: 'transparent'
	      };
	
	      return _react2.default.createElement(
	        'div',
	        { className: _Spinner2.default.wrapper, style: wrapperStyle },
	        _react2.default.createElement(
	          'div',
	          { className: _Spinner2.default.hoopWrapper, style: sizeStyle },
	          _react2.default.createElement(
	            'div',
	            { className: _Spinner2.default.hoopIcon, style: sizeStyle },
	            _react2.default.createElement(
	              'div',
	              { className: _Spinner2.default.hoopCut, style: cutStyle },
	              _react2.default.createElement('div', { className: _Spinner2.default.hoopDonut, style: donutStyle })
	            )
	          )
	        )
	      );
	    }
	  }]);
	
	  return Spinner;
	}(_react2.default.Component), _class.propTypes = {
	  size: _react.PropTypes.oneOfType([_react.PropTypes.number, _react.PropTypes.string]),
	  width: _react.PropTypes.oneOfType([_react.PropTypes.number, _react.PropTypes.string]),
	  color: _react.PropTypes.string,
	  bgColor: _react.PropTypes.string
	}, _temp);
	exports.default = Spinner;

/***/ },
/* 59 */
/***/ function(module, exports) {

	module.exports = {
		"wrapper": "Spinner__wrapper__tio6b",
		"hoopWrapper": "Spinner__hoopWrapper__3VF8o",
		"hoopIcon": "Spinner__hoopIcon__3EqH-",
		"clockwise": "Spinner__clockwise__xv_wY",
		"hoopCut": "Spinner__hoopCut__2hlG1",
		"hoopDonut": "Spinner__hoopDonut__CGIKb",
		"donut-rotate": "Spinner__donut-rotate__2-PFS"
	};

/***/ },
/* 60 */
/***/ function(module, exports) {

	module.exports = {
		"dummiesSection": "Dummies__dummiesSection__9W2_h",
		"content": "Dummies__content__1wcOx",
		"loggedIn": "Dummies__loggedIn__3NZJF",
		"loggedOut": "Dummies__loggedOut__7kWyJ",
		"authMessage": "Dummies__authMessage__11RjH",
		"authButtonWrapper": "Dummies__authButtonWrapper__1gn6_",
		"authButton": "Dummies__authButton__3HWTC",
		"dummyCard": "Dummies__dummyCard__2pu9Z"
	};

/***/ },
/* 61 */
/***/ function(module, exports, __webpack_require__) {

	'use strict';
	
	Object.defineProperty(exports, "__esModule", {
	  value: true
	});
	exports.default = undefined;
	
	var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();
	
	var _dec, _class;
	
	var _react = __webpack_require__(15);
	
	var _react2 = _interopRequireDefault(_react);
	
	var _redux = __webpack_require__(23);
	
	var _reactRedux = __webpack_require__(18);
	
	var _Login = __webpack_require__(62);
	
	var _Login2 = _interopRequireDefault(_Login);
	
	var _authActions = __webpack_require__(44);
	
	var authActions = _interopRequireWildcard(_authActions);
	
	function _interopRequireWildcard(obj) { if (obj && obj.__esModule) { return obj; } else { var newObj = {}; if (obj != null) { for (var key in obj) { if (Object.prototype.hasOwnProperty.call(obj, key)) newObj[key] = obj[key]; } } newObj.default = obj; return newObj; } }
	
	function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }
	
	function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }
	
	function _possibleConstructorReturn(self, call) { if (!self) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return call && (typeof call === "object" || typeof call === "function") ? call : self; }
	
	function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function, not " + typeof superClass); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, enumerable: false, writable: true, configurable: true } }); if (superClass) Object.setPrototypeOf ? Object.setPrototypeOf(subClass, superClass) : subClass.__proto__ = superClass; }
	
	var stateMap = function stateMap(state) {
	  return {
	    $authStore: state.$authStore
	  };
	};
	
	var actionsMap = function actionsMap(dispatch) {
	  return {
	    authActions: (0, _redux.bindActionCreators)(authActions, dispatch)
	  };
	};
	
	var LoginContainer = (_dec = (0, _reactRedux.connect)(stateMap, actionsMap), _dec(_class = function (_React$Component) {
	  _inherits(LoginContainer, _React$Component);
	
	  function LoginContainer() {
	    _classCallCheck(this, LoginContainer);
	
	    return _possibleConstructorReturn(this, Object.getPrototypeOf(LoginContainer).apply(this, arguments));
	  }
	
	  _createClass(LoginContainer, [{
	    key: 'render',
	    value: function render() {
	      return _react2.default.createElement(_Login2.default, this.props);
	    }
	  }], [{
	    key: 'checkAuth',
	    value: function checkAuth(nextState, replaceState) {
	      var _store$getState = this.store.getState();
	
	      var $authStore = _store$getState.$authStore;
	
	      if ($authStore.get('isLoggedIn')) replaceState(null, '/');
	    }
	  }]);
	
	  return LoginContainer;
	}(_react2.default.Component)) || _class);
	exports.default = LoginContainer;

/***/ },
/* 62 */
/***/ function(module, exports, __webpack_require__) {

	'use strict';
	
	Object.defineProperty(exports, "__esModule", {
	  value: true
	});
	exports.default = undefined;
	
	var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();
	
	var _class, _temp;
	
	var _react = __webpack_require__(15);
	
	var _react2 = _interopRequireDefault(_react);
	
	var _immutable = __webpack_require__(37);
	
	var _immutable2 = _interopRequireDefault(_immutable);
	
	var _bindToContext = __webpack_require__(57);
	
	var _bindToContext2 = _interopRequireDefault(_bindToContext);
	
	var _animate = __webpack_require__(63);
	
	var _animate2 = _interopRequireDefault(_animate);
	
	var _Button = __webpack_require__(65);
	
	var _Button2 = _interopRequireDefault(_Button);
	
	var _authConstants = __webpack_require__(46);
	
	var _authConstants2 = _interopRequireDefault(_authConstants);
	
	var _Login = __webpack_require__(67);
	
	var _Login2 = _interopRequireDefault(_Login);
	
	function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }
	
	function _defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }
	
	function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }
	
	function _possibleConstructorReturn(self, call) { if (!self) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return call && (typeof call === "object" || typeof call === "function") ? call : self; }
	
	function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function, not " + typeof superClass); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, enumerable: false, writable: true, configurable: true } }); if (superClass) Object.setPrototypeOf ? Object.setPrototypeOf(subClass, superClass) : subClass.__proto__ = superClass; }
	
	// TODO: This form is copy/paste from old non-redux project.
	//       Rewrite it for redux.
	var Login = (_temp = _class = function (_React$Component) {
	  _inherits(Login, _React$Component);
	
	  function Login(props, context) {
	    _classCallCheck(this, Login);
	
	    var _this = _possibleConstructorReturn(this, Object.getPrototypeOf(Login).call(this, props, context));
	
	    _this.state = {
	      login: null,
	      loginState: null,
	      password: null,
	      passwordState: null
	    };
	
	    (0, _bindToContext2.default)(['_handleValueChange', '_validateForm', '_setFormState', '_handleSuccessSubmit', '_handleFailedSubmit'], _this);
	    return _this;
	  }
	
	  _createClass(Login, [{
	    key: 'componentDidMount',
	    value: function componentDidMount() {
	      var _props = this.props;
	      var loadingProgress = _props.loadingProgress;
	      var setTitle = _props.setTitle;
	
	      loadingProgress.done();
	      setTitle.call(this);
	    }
	  }, {
	    key: 'componentWillReceiveProps',
	    value: function componentWillReceiveProps(newProps) {
	      var $authStore = newProps.$authStore;
	
	      var action = $authStore.get('action');
	
	      if (action === _authConstants2.default.AUTH_LOGIN_FAILED) {
	        this._handleFailedSubmit();
	      }
	    }
	  }, {
	    key: '_handleValueChange',
	    value: function _handleValueChange(e) {
	      var _e$target = e.target;
	      var name = _e$target.name;
	      var value = _e$target.value;
	
	
	      this.setState(_defineProperty({}, name, value.trim()), this._validateForm.bind(this, null, name));
	    }
	  }, {
	    key: '_validateForm',
	    value: function _validateForm(e, attr) {
	      var _this2 = this;
	
	      var rules = {
	        login: {
	          required: true
	        },
	        password: {
	          required: true
	        }
	      };
	
	      var isFail = _Login2.default.error;
	      var isOk = _Login2.default.ok;
	
	      var isSubmit = e ? e.target.nodeName === 'FORM' : false;
	
	      this._formIsValid = true;
	      this._focusedOnError = false;
	      this._wasSubmitted = false;
	
	      if (isSubmit && !this._wasSubmitted) this._wasSubmitted = true;
	
	      var attributes = [];
	
	      if (isSubmit) {
	        e.preventDefault();
	        for (var attribute in rules) {
	          if (rules.hasOwnProperty(attribute)) {
	            attributes.push({
	              key: attribute,
	              rules: rules[attribute]
	            });
	          }
	        }
	      } else {
	        attributes.push({
	          key: attr,
	          rules: rules[attr]
	        });
	      }
	
	      attributes.forEach(function (attribute, index) {
	        var attrValue = _this2.state[attribute.key] ? _this2.state[attribute.key].trim() : _this2.state[attribute.key];
	        var attrRules = attribute.rules;
	
	        if (attrRules.required && !attrValue) {
	          _this2._setFormState(attribute.key, isFail);
	          return;
	        }
	
	        _this2._setFormState(attribute.key, isOk);
	
	        if (isSubmit && _this2._formIsValid && index === attributes.length - 1) {
	          _this2._handleSuccessSubmit();
	        }
	      });
	
	      if (isSubmit && !this._formIsValid) {
	        this._handleFailedSubmit();
	      }
	    }
	  }, {
	    key: '_setFormState',
	    value: function _setFormState(attr, status) {
	      var isFail = status === _Login2.default.error;
	
	      this.setState(_defineProperty({}, attr + 'Status', status));
	
	      if (isFail) {
	        this._formIsValid = false;
	        if (!this._focusedOnError) {
	          this.refs[attr].focus();
	          this._focusedOnError = true;
	        }
	      }
	    }
	  }, {
	    key: '_handleSuccessSubmit',
	    value: function _handleSuccessSubmit() {
	      var _state = this.state;
	      var login = _state.login;
	      var password = _state.password;
	      var _props2 = this.props;
	      var authAgent = _props2.authAgent;
	      var authActions = _props2.authActions;
	      var history = _props2.history;
	
	
	      var authData = {
	        'api_user': {
	          'email': login,
	          'password': password
	        }
	      };
	
	      authActions.login({ authData: authData, authAgent: authAgent, history: history });
	    }
	  }, {
	    key: '_handleFailedSubmit',
	    value: function _handleFailedSubmit() {
	      (0, _animate2.default)('login-form', 'shake');
	    }
	  }, {
	    key: 'render',
	    value: function render() {
	      var $authStore = this.props.$authStore;
	
	      var isLoading = $authStore.get('isLoading');
	
	      return _react2.default.createElement(
	        'section',
	        { className: _Login2.default.loginSection },
	        _react2.default.createElement(
	          'form',
	          { id: 'login-form', onSubmit: this._validateForm },
	          _react2.default.createElement('input', {
	            type: 'text',
	            ref: 'login',
	            name: 'login',
	            placeholder: 'Email',
	            value: this.state.login,
	            className: this.state.loginStatus,
	            onChange: this._handleValueChange,
	            autoFocus: true
	          }),
	          _react2.default.createElement('input', {
	            type: 'password',
	            ref: 'password',
	            name: 'password',
	            placeholder: 'Password',
	            value: this.state.password,
	            className: this.state.passwordStatus,
	            onChange: this._handleValueChange
	          }),
	          _react2.default.createElement(
	            _Button2.default,
	            {
	              wrapperClassName: _Login2.default.buttonWrapper,
	              spinnerSize: '20',
	              spinnerColor: '#fff',
	              isProcessing: isLoading
	            },
	            'Login!'
	          )
	        )
	      );
	    }
	  }]);
	
	  return Login;
	}(_react2.default.Component), _class.propTypes = {
	  $authStore: _react.PropTypes.instanceOf(_immutable2.default.Map).isRequired,
	
	  authActions: _react.PropTypes.shape({
	    login: _react.PropTypes.func.isRequired
	  }).isRequired,
	
	  authAgent: _react.PropTypes.shape({
	    login: _react.PropTypes.func.isRequired,
	    logout: _react.PropTypes.func.isRequired
	  }),
	
	  history: _react.PropTypes.object.isRequired,
	  loadingProgress: _react.PropTypes.object.isRequired,
	  setTitle: _react.PropTypes.func.isRequired
	}, _temp);
	exports.default = Login;

/***/ },
/* 63 */
/***/ function(module, exports, __webpack_require__) {

	'use strict';
	
	Object.defineProperty(exports, "__esModule", {
	  value: true
	});
	
	var _className = __webpack_require__(64);
	
	var _className2 = _interopRequireDefault(_className);
	
	function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }
	
	exports.default = function (element, animation, cb) {
	
	  /* Get element */
	
	  var node = typeof element === 'string' ? document.getElementById(element) : element;
	
	  /* Detect events */
	
	  var endEvents = [];
	
	  var EVENTS_MAP = {
	
	    transitionEndEvents: {
	      'transition': 'transitionend',
	      'WebkitTransition': 'webkitTransitionEnd',
	      'MozTransition': 'mozTransitionEnd',
	      'OTransition': 'oTransitionEnd',
	      'msTransition': 'MSTransitionEnd'
	    },
	
	    animationEndEvents: {
	      'animation': 'animationend',
	      'WebkitAnimation': 'webkitAnimationEnd',
	      'MozAnimation': 'mozAnimationEnd',
	      'OAnimation': 'oAnimationEnd',
	      'msAnimation': 'MSAnimationEnd'
	    }
	
	  };
	
	  var testEl = document.createElement('div');
	  var style = testEl.style;
	
	  if (!('AnimationEvent' in window)) {
	    delete EVENTS_MAP.animationEndEvents.animation;
	  }
	
	  if (!('TransitionEvent' in window)) {
	    delete EVENTS_MAP.transitionEndEvents.transition;
	  }
	
	  for (var baseEventName in EVENTS_MAP) {
	    if (EVENTS_MAP.hasOwnProperty(baseEventName)) {
	      var baseEvents = EVENTS_MAP[baseEventName];
	      for (var styleName in baseEvents) {
	        if (baseEvents.hasOwnProperty(styleName) && styleName in style) {
	          endEvents.push(baseEvents[styleName]);
	          break;
	        }
	      }
	    }
	  }
	
	  /* Define events handlers */
	
	  var eventHandlers = {
	    addEndEventListener: function addEndEventListener(target, eventListener) {
	
	      if (endEvents.length === 0) {
	        window.setTimeout(eventListener, 0);
	        return;
	      }
	
	      endEvents.forEach(function (endEvent) {
	        target.addEventListener(endEvent, eventListener, false);
	      });
	    },
	    removeEndEventListener: function removeEndEventListener(target, eventListener) {
	
	      if (endEvents.length === 0) return;
	
	      endEvents.forEach(function (endEvent) {
	        target.removeEventListener(endEvent, eventListener, false);
	      });
	    }
	  };
	
	  /* Animate */
	
	  var classNames = [animation, 'animated'];
	
	  var endListener = function endListener(e) {
	    if (e && e.target !== node) return;
	    eventHandlers.removeEndEventListener(node, endListener);
	    _className2.default.remove(node, classNames);
	    if (cb) return cb();
	  };
	
	  _className2.default.remove(node, classNames);
	  _className2.default.add(node, classNames);
	
	  eventHandlers.addEndEventListener(node, endListener);
	};

/***/ },
/* 64 */
/***/ function(module, exports) {

	'use strict';
	
	Object.defineProperty(exports, "__esModule", {
	  value: true
	});
	exports.default = {
	  add: function add(element, classNames) {
	    var _this = this;
	
	    if (element.classList) {
	      this._iterateClassNames(classNames, function (className) {
	        if (!_this._hasClass(element, className)) {
	          element.classList.add(className);
	        }
	      });
	    } else {
	      this._iterateClassNames(classNames, function (className) {
	        if (!_this._hasClass(element, className)) {
	          element.className = element.className + ' ' + className;
	        }
	      });
	    }
	    return element;
	  },
	  remove: function remove(element, classNames) {
	    var _this2 = this;
	
	    if (element.classList) {
	      this._iterateClassNames(classNames, function (className) {
	        if (_this2._hasClass(element, className)) {
	          element.classList.remove(className);
	        }
	      });
	    } else {
	      this._iterateClassNames(classNames, function (className) {
	        if (_this2._hasClass(element, className)) {
	          element.className = (' ' + element.className + ' ').replace(' ' + className + ' ', ' ').trim();
	        }
	      });
	    }
	    return element;
	  },
	  _hasClass: function _hasClass(element, className) {
	    if (element.classList) {
	      return element.classList.contains(className);
	    }
	    return (' ' + element.className + ' ').indexOf(' ' + className + ' ') > -1;
	  },
	  _iterateClassNames: function _iterateClassNames(classNames, cb) {
	    if (Array.isArray(classNames)) {
	      classNames.forEach(function (className) {
	        return cb(className);
	      });
	    } else {
	      return cb(classNames);
	    }
	  }
	};

/***/ },
/* 65 */
/***/ function(module, exports, __webpack_require__) {

	'use strict';
	
	Object.defineProperty(exports, "__esModule", {
	  value: true
	});
	exports.default = undefined;
	
	var _extends = Object.assign || function (target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i]; for (var key in source) { if (Object.prototype.hasOwnProperty.call(source, key)) { target[key] = source[key]; } } } return target; };
	
	var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();
	
	var _class, _temp;
	
	var _react = __webpack_require__(15);
	
	var _react2 = _interopRequireDefault(_react);
	
	var _Spinner = __webpack_require__(58);
	
	var _Spinner2 = _interopRequireDefault(_Spinner);
	
	var _Button = __webpack_require__(66);
	
	var _Button2 = _interopRequireDefault(_Button);
	
	function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }
	
	function _objectWithoutProperties(obj, keys) { var target = {}; for (var i in obj) { if (keys.indexOf(i) >= 0) continue; if (!Object.prototype.hasOwnProperty.call(obj, i)) continue; target[i] = obj[i]; } return target; }
	
	function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }
	
	function _possibleConstructorReturn(self, call) { if (!self) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return call && (typeof call === "object" || typeof call === "function") ? call : self; }
	
	function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function, not " + typeof superClass); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, enumerable: false, writable: true, configurable: true } }); if (superClass) Object.setPrototypeOf ? Object.setPrototypeOf(subClass, superClass) : subClass.__proto__ = superClass; }
	
	var Button = (_temp = _class = function (_React$Component) {
	  _inherits(Button, _React$Component);
	
	  function Button() {
	    _classCallCheck(this, Button);
	
	    return _possibleConstructorReturn(this, Object.getPrototypeOf(Button).apply(this, arguments));
	  }
	
	  _createClass(Button, [{
	    key: 'render',
	    value: function render() {
	      var _props = this.props;
	      var wrapperClassName = _props.wrapperClassName;
	      var wrapperStyle = _props.wrapperStyle;
	      var className = _props.className;
	      var style = _props.style;
	      var children = _props.children;
	      var onClick = _props.onClick;
	      var isProcessing = _props.isProcessing;
	      var useSpan = _props.useSpan;
	      var spinnerSize = _props.spinnerSize;
	      var spinnerWidth = _props.spinnerWidth;
	      var spinnerColor = _props.spinnerColor;
	
	      var rest = _objectWithoutProperties(_props, ['wrapperClassName', 'wrapperStyle', 'className', 'style', 'children', 'onClick', 'isProcessing', 'useSpan', 'spinnerSize', 'spinnerWidth', 'spinnerColor']);
	
	      var wrapperProps = {
	        className: wrapperClassName || _Button2.default.wrapper,
	        style: wrapperStyle || null
	      };
	
	      var buttonProps = {
	        className: className || _Button2.default.button,
	        style: style || null,
	        disabled: isProcessing || false,
	        onClick: onClick || null
	      };
	
	      var spinnerProps = {
	        size: spinnerSize || null,
	        width: spinnerWidth || null,
	        color: spinnerColor || null
	      };
	
	      return _react2.default.createElement(
	        'div',
	        wrapperProps,
	        useSpan ? _react2.default.createElement(
	          'span',
	          _extends({}, buttonProps, rest),
	          children
	        ) : _react2.default.createElement(
	          'button',
	          _extends({}, buttonProps, rest),
	          children
	        ),
	        isProcessing && _react2.default.createElement(_Spinner2.default, spinnerProps)
	      );
	    }
	  }]);
	
	  return Button;
	}(_react2.default.Component), _class.propTypes = {
	  wrapperClassName: _react.PropTypes.string,
	  wrapperStyle: _react.PropTypes.object,
	
	  className: _react.PropTypes.string,
	  style: _react.PropTypes.object,
	  children: _react.PropTypes.string.isRequired,
	  onClick: _react.PropTypes.func,
	  isProcessing: _react.PropTypes.bool,
	  useSpan: _react.PropTypes.bool,
	
	  spinnerColor: _react.PropTypes.string,
	  spinnerSize: _react.PropTypes.oneOfType([_react.PropTypes.number, _react.PropTypes.string]),
	  spinnerWidth: _react.PropTypes.oneOfType([_react.PropTypes.number, _react.PropTypes.string])
	}, _temp);
	exports.default = Button;

/***/ },
/* 66 */
/***/ function(module, exports) {

	module.exports = {
		"wrapper": "Button__wrapper__18I6_",
		"button": "Button__button__3OiPd"
	};

/***/ },
/* 67 */
/***/ function(module, exports) {

	module.exports = {
		"loginSection": "Login__loginSection__3kj9-",
		"error": "Login__error__6Omdc",
		"ok": "Login__ok__2nAQW",
		"buttonWrapper": "Login__buttonWrapper__f6PW2"
	};

/***/ },
/* 68 */
/***/ function(module, exports, __webpack_require__) {

	'use strict';
	
	Object.defineProperty(exports, "__esModule", {
	  value: true
	});
	exports.default = undefined;
	
	var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();
	
	var _class, _temp;
	
	var _react = __webpack_require__(15);
	
	var _react2 = _interopRequireDefault(_react);
	
	function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }
	
	function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }
	
	function _possibleConstructorReturn(self, call) { if (!self) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return call && (typeof call === "object" || typeof call === "function") ? call : self; }
	
	function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function, not " + typeof superClass); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, enumerable: false, writable: true, configurable: true } }); if (superClass) Object.setPrototypeOf ? Object.setPrototypeOf(subClass, superClass) : subClass.__proto__ = superClass; }
	
	var Logout = (_temp = _class = function (_React$Component) {
	  _inherits(Logout, _React$Component);
	
	  function Logout() {
	    _classCallCheck(this, Logout);
	
	    return _possibleConstructorReturn(this, Object.getPrototypeOf(Logout).apply(this, arguments));
	  }
	
	  _createClass(Logout, [{
	    key: 'componentDidMount',
	    value: function componentDidMount() {
	      var history = this.props.history;
	      var nextLocation = this.props.location.state.nextLocation;
	
	
	      history.pushState(null, nextLocation || '/');
	    }
	  }, {
	    key: 'render',
	    value: function render() {
	      return null;
	    }
	  }]);
	
	  return Logout;
	}(_react2.default.Component), _class.propTypes = {
	  history: _react.PropTypes.shape({
	    pushState: _react.PropTypes.func.isRequired
	  }).isRequired,
	  location: _react.PropTypes.shape({
	    state: _react.PropTypes.object.isRequired
	  }).isRequired
	}, _temp);
	exports.default = Logout;

/***/ },
/* 69 */
/***/ function(module, exports, __webpack_require__) {

	'use strict';
	
	Object.defineProperty(exports, "__esModule", {
	  value: true
	});
	exports.default = undefined;
	
	var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();
	
	var _react = __webpack_require__(15);
	
	var _react2 = _interopRequireDefault(_react);
	
	var _RequestError = __webpack_require__(70);
	
	var _RequestError2 = _interopRequireDefault(_RequestError);
	
	function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }
	
	function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }
	
	function _possibleConstructorReturn(self, call) { if (!self) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return call && (typeof call === "object" || typeof call === "function") ? call : self; }
	
	function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function, not " + typeof superClass); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, enumerable: false, writable: true, configurable: true } }); if (superClass) Object.setPrototypeOf ? Object.setPrototypeOf(subClass, superClass) : subClass.__proto__ = superClass; }
	
	var NotFound = function (_React$Component) {
	  _inherits(NotFound, _React$Component);
	
	  function NotFound() {
	    _classCallCheck(this, NotFound);
	
	    return _possibleConstructorReturn(this, Object.getPrototypeOf(NotFound).apply(this, arguments));
	  }
	
	  _createClass(NotFound, [{
	    key: 'render',
	    value: function render() {
	      // FIXME: This should throw 404 on server
	      //        and render `not found` view on client
	      throw new _RequestError2.default(404);
	    }
	  }]);
	
	  return NotFound;
	}(_react2.default.Component);
	
	exports.default = NotFound;

/***/ },
/* 70 */
/***/ function(module, exports) {

	"use strict";
	
	Object.defineProperty(exports, "__esModule", {
	  value: true
	});
	var RequestError = function RequestError(status, message) {
	  this.status = status;
	  this.message = message || null;
	};
	
	RequestError.prototype = Object.create(Error.prototype);
	RequestError.prototype.constructor = RequestError;
	
	exports.default = RequestError;

/***/ },
/* 71 */
/***/ function(module, exports, __webpack_require__) {

	'use strict';
	
	Object.defineProperty(exports, "__esModule", {
	  value: true
	});
	
	var _authReducer = __webpack_require__(72);
	
	var _authReducer2 = _interopRequireDefault(_authReducer);
	
	var _dummiesReducer = __webpack_require__(73);
	
	var _dummiesReducer2 = _interopRequireDefault(_dummiesReducer);
	
	function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }
	
	exports.default = {
	  $authStore: _authReducer2.default,
	  $dummiesStore: _dummiesReducer2.default
	};

/***/ },
/* 72 */
/***/ function(module, exports, __webpack_require__) {

	'use strict';
	
	Object.defineProperty(exports, "__esModule", {
	  value: true
	});
	exports.default = authReducer;
	
	var _immutable = __webpack_require__(37);
	
	var _immutable2 = _interopRequireDefault(_immutable);
	
	var _authConstants = __webpack_require__(46);
	
	var _authConstants2 = _interopRequireDefault(_authConstants);
	
	function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }
	
	var $initialState = _immutable2.default.fromJS({
	  action: null,
	  user: null,
	  isLoggedIn: false,
	  errors: null,
	  isLoading: false
	});
	
	function authReducer() {
	  var $state = arguments.length <= 0 || arguments[0] === undefined ? $initialState : arguments[0];
	  var _ref = arguments[1];
	  var type = _ref.type;
	  var user = _ref.user;
	  var errors = _ref.errors;
	
	
	  switch (type) {
	
	    case _authConstants2.default.AUTH_LOGGED_IN:
	      {
	        return $state.merge({
	          action: type,
	          user: user,
	          isLoggedIn: true,
	          errors: null,
	          isLoading: false
	        });
	      }
	
	    case _authConstants2.default.AUTH_LOGIN_REQUESTED:
	      {
	        return $state.merge({
	          action: type,
	          user: null,
	          isLoggedIn: false,
	          errors: null,
	          isLoading: true
	        });
	      }
	
	    case _authConstants2.default.AUTH_LOGIN_SUCCEED:
	      {
	        return $state.merge({
	          action: type,
	          user: user,
	          isLoggedIn: true,
	          errors: null,
	          isLoading: false
	        });
	      }
	
	    case _authConstants2.default.AUTH_LOGIN_FAILED:
	      {
	        return $state.merge({
	          action: type,
	          user: null,
	          isLoggedIn: false,
	          errors: errors,
	          isLoading: false
	        });
	      }
	
	    case _authConstants2.default.AUTH_LOGGED_OUT:
	      {
	        return $state.merge({
	          action: type,
	          user: null,
	          isLoggedIn: false,
	          errors: null,
	          isLoading: false
	        });
	      }
	
	    default:
	      {
	        return $state;
	      }
	
	  }
	}

/***/ },
/* 73 */
/***/ function(module, exports, __webpack_require__) {

	'use strict';
	
	Object.defineProperty(exports, "__esModule", {
	  value: true
	});
	exports.default = dummiesReducer;
	
	var _immutable = __webpack_require__(37);
	
	var _immutable2 = _interopRequireDefault(_immutable);
	
	var _dummiesConstants = __webpack_require__(54);
	
	var _dummiesConstants2 = _interopRequireDefault(_dummiesConstants);
	
	function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }
	
	var $initialState = _immutable2.default.fromJS({
	  action: null,
	  index: new _immutable.Set(),
	  entities: {},
	  errors: null,
	  isLoading: false
	});
	
	function dummiesReducer() {
	  var $state = arguments.length <= 0 || arguments[0] === undefined ? $initialState : arguments[0];
	  var _ref = arguments[1];
	  var type = _ref.type;
	  var index = _ref.index;
	  var entities = _ref.entities;
	  var errors = _ref.errors;
	
	
	  switch (type) {
	
	    case _dummiesConstants2.default.DUMMY_LOAD_LIST_REQUESTED:
	      {
	        return $state.merge({
	          action: type,
	          isLoading: true
	        });
	      }
	
	    case _dummiesConstants2.default.DUMMY_LOAD_LIST_SUCCEED:
	      {
	        return $state.merge({
	          action: type,
	          entities: entities,
	          index: new _immutable.Set(index),
	          isLoading: false
	        });
	      }
	
	    case _dummiesConstants2.default.DUMMY_LOAD_LIST_FAILED:
	      {
	        return $state.merge({
	          action: type,
	          errors: errors,
	          isLoading: false
	        });
	      }
	
	    default:
	      {
	        return $state;
	      }
	
	  }
	}

/***/ },
/* 74 */
/***/ function(module, exports, __webpack_require__) {

	'use strict';
	
	Object.defineProperty(exports, "__esModule", {
	  value: true
	});
	exports.default = undefined;
	
	var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();
	
	var _react = __webpack_require__(15);
	
	var _react2 = _interopRequireDefault(_react);
	
	var _getMeta2 = __webpack_require__(75);
	
	var _getMeta3 = _interopRequireDefault(_getMeta2);
	
	var _images = __webpack_require__(76);
	
	var _images2 = _interopRequireDefault(_images);
	
	function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }
	
	function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }
	
	function _possibleConstructorReturn(self, call) { if (!self) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return call && (typeof call === "object" || typeof call === "function") ? call : self; }
	
	function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function, not " + typeof superClass); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, enumerable: false, writable: true, configurable: true } }); if (superClass) Object.setPrototypeOf ? Object.setPrototypeOf(subClass, superClass) : subClass.__proto__ = superClass; }
	
	var Head = function (_React$Component) {
	  _inherits(Head, _React$Component);
	
	  function Head() {
	    _classCallCheck(this, Head);
	
	    return _possibleConstructorReturn(this, Object.getPrototypeOf(Head).apply(this, arguments));
	  }
	
	  _createClass(Head, [{
	    key: 'render',
	    value: function render() {
	      var _getMeta = (0, _getMeta3.default)(this.props);
	
	      var title = _getMeta.title;
	      var description = _getMeta.description;
	      var keywords = _getMeta.keywords;
	      var type = _getMeta.type;
	      var image = _getMeta.image;
	      var siteName = _getMeta.siteName;
	      var url = _getMeta.url;
	      var facebookAppId = _getMeta.facebookAppId;
	      var cssAsset = _getMeta.cssAsset;
	
	
	      return _react2.default.createElement(
	        'head',
	        null,
	        _react2.default.createElement('base', { href: '/' }),
	        _react2.default.createElement('meta', { charSet: 'utf-8' }),
	        _react2.default.createElement(
	          'title',
	          null,
	          title
	        ),
	        _react2.default.createElement('meta', { name: 'viewport', content: 'width=device-width,initial-scale=1' }),
	        !(true) && _react2.default.createElement('link', { rel: 'stylesheet', href: cssAsset }),
	        _react2.default.createElement('meta', { name: 'description', content: description }),
	        _react2.default.createElement('meta', { name: 'keywords', content: keywords }),
	        _react2.default.createElement('meta', { property: 'og:type', content: type }),
	        _react2.default.createElement('meta', { property: 'og:title', content: title }),
	        _react2.default.createElement('meta', { property: 'og:site_name', content: siteName }),
	        _react2.default.createElement('meta', { property: 'og:url', content: url }),
	        _react2.default.createElement('meta', { property: 'og:description', content: description }),
	        _react2.default.createElement('meta', { property: 'og:image', content: image }),
	        _react2.default.createElement('meta', { property: 'fb:app_id', content: facebookAppId }),
	        _react2.default.createElement('link', { rel: 'shortcut icon', href: '/assets/' + _images2.default.favicon }),
	        _react2.default.createElement('link', { rel: 'apple-touch-icon', href: '/assets/' + _images2.default.appleIcon })
	      );
	    }
	  }]);
	
	  return Head;
	}(_react2.default.Component);
	
	exports.default = Head;

/***/ },
/* 75 */
/***/ function(module, exports) {

	'use strict';
	
	Object.defineProperty(exports, "__esModule", {
	  value: true
	});
	
	/*
	 * ===== Meta
	 *
	 * title
	 * description
	 * keywords
	 * type
	 * url
	 * image
	 * siteName
	 * appHost
	 * cssAsset
	 * facebookAppId
	 *
	 */
	
	exports.default = function (props) {
	
	  var meta = {};
	
	  var route = props.route;
	  var appHost = props.appHost;
	  var fullPath = props.fullPath;
	  var cssAsset = props.cssAsset;
	  var facebookAppId = props.facebookAppId;
	
	
	  var base = {
	    title: 'Flux on Rails scaffolder',
	    description: 'Will scaffold Flux on Rails App for you.',
	    keywords: 'flux react redux rails',
	    type: 'website',
	    image: '/images/cover.png',
	    siteName: 'Flux on Rails scaffolder',
	    facebookAppId: facebookAppId || null
	  };
	
	  meta.cssAsset = cssAsset;
	  meta.appHost = appHost;
	
	  meta.url = appHost + fullPath;
	  meta.image = appHost + base.image;
	
	  meta.type = base.type;
	  meta.siteName = base.siteName;
	  meta.facebookAppId = base.facebookAppId;
	
	  switch (route) {
	
	    case 'dummies':
	
	      meta.title = 'Dummy title';
	      meta.description = base.description;
	      meta.keywords = base.keywords;
	      break;
	
	    case 'login':
	      meta.title = 'Login';
	      meta.description = base.description;
	      meta.keywords = base.keywords;
	      break;
	
	    case 'not-found':
	      meta.title = 'Oops! Nothing here.';
	      meta.description = '404';
	      meta.keywords = base.keywords;
	      break;
	
	    default:
	      meta.title = base.title;
	      meta.description = base.description;
	      meta.keywords = base.keywords;
	
	  }
	
	  if (meta.title !== base.title) {
	    meta.title += ' | ' + base.title;
	  }
	
	  return meta;
	};

/***/ },
/* 76 */
/***/ function(module, exports, __webpack_require__) {

	'use strict';
	
	Object.defineProperty(exports, "__esModule", {
	  value: true
	});
	
	var _favicon = __webpack_require__(77);
	
	var _favicon2 = _interopRequireDefault(_favicon);
	
	var _appleTouchIcon = __webpack_require__(78);
	
	var _appleTouchIcon2 = _interopRequireDefault(_appleTouchIcon);
	
	function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }
	
	exports.default = { favicon: _favicon2.default, appleIcon: _appleTouchIcon2.default };

/***/ },
/* 77 */
/***/ function(module, exports, __webpack_require__) {

	module.exports = __webpack_require__.p + "favicon-23b1d2268f1a27f173fac73cfce0cf53.ico";

/***/ },
/* 78 */
/***/ function(module, exports, __webpack_require__) {

	module.exports = __webpack_require__.p + "apple-touch-icon-15a532abafa66fd9a64c64955202b4a0.png";

/***/ },
/* 79 */
/***/ function(module, exports, __webpack_require__) {

	'use strict';
	
	Object.defineProperty(exports, "__esModule", {
	  value: true
	});
	
	var _server = __webpack_require__(28);
	
	var _server2 = _interopRequireDefault(_server);
	
	var _manifest = __webpack_require__(80);
	
	var _manifest2 = _interopRequireDefault(_manifest);
	
	function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }
	
	exports.default = function (asset, type) {
	
	  if (true) {
	    return 'http://lvh.me:' + _server2.default.devPort + '/assets/' + asset + '.' + type;
	  } else {
	    return '/assets/' + _manifest2.default[asset + '.' + type];
	  }
	};

/***/ },
/* 80 */
/***/ function(module, exports) {

	module.exports = {};

/***/ },
/* 81 */
/***/ function(module, exports) {

	'use strict';
	
	Object.defineProperty(exports, "__esModule", {
	  value: true
	});
	
	exports.default = function (host, periods) {
	
	  var periodsAmount = periods ? periods + 1 : 2;
	
	  var domain = host.split('.').slice(-periodsAmount).join('.');
	
	  return '.' + domain;
	};

/***/ }
/******/ ])));
//# sourceMappingURL=app.js.map